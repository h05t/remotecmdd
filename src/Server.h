#ifndef SERVER_H
#define SERVER_H

#include <vector>
#include <string>
#include <memory>

#include <boost/thread.hpp>
#include <boost/asio.hpp>

#include "Session.h"
#include "CmdConfigParser.h"


using namespace std;

//
// Main daemon class as a Server or boost::asio::acceptor
//
class Server
{
public:
    Server(short port, size_t thread_pool_size, const char* cmd_config_file, size_t timeout);

    /* Noncopyable */
    Server(const Server&) = delete;
    Server& operator =(const Server&) = delete;

    // run server
    void run();
    // Dispatches by child pid and handles child exit
    void process_child_exit(pid_t pid);

private:
    size_t m_thread_pool_size;
    size_t m_timeout;

    boost::asio::io_service m_io_service;           // init before handling
    boost::asio::signal_set m_quit_signals;         // Signal sets for handling signals (signal for async wait)
    boost::asio::signal_set m_update_config_signal; // signal for async wait when config update
    boost::asio::ip::tcp::acceptor m_tcp_acceptor;  // master socket that get clients connections
    boost::asio::ip::tcp::endpoint m_tcp_endpoint;  // sockaddr

    CmdConfigParser m_config_parser;                // config parser
    map<string, string> m_config;    // config data
    boost::shared_mutex m_config_mutex;             // config locker

    // SIGCHLD dispatching stuff
    map<pid_t, shared_ptr<BaseSession>> m_pid_to_session_map;   // session by pid
    boost::mutex m_signal_mutex;

    // connect hanlders
    void tcp_accept();
    void configure_tcp_endpoint();
    //
    void handle_update_config();
    void handle_stop();             // stop server
};

#endif // SERVER_H
