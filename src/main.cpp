#include <csignal>
#include <iostream>
#include <fstream>

#include <boost/lexical_cast.hpp>

#include "Server.h"


static const char* config_file_name = "/etc/rcmd.conf";         // daemon config file
static const size_t server_thread_pool_size = 5;                // pool size
static const size_t port = 12345;                               // daemon port

std::shared_ptr<Server> server_ptr;

void child_exit_handler(int signum, siginfo_t* info, void* context)
{
    server_ptr->process_child_exit(info->si_pid);
}
//
void help()
{
    std::cout << "USAGE: ./RCMD <config_file> <timeout>" << std::endl;
}
//
int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        help();
        exit(0);
    }

    config_file_name = argv[1];
    std::ifstream config(config_file_name);
    if (!config)
    {
        std::cerr << "Config file does not exist!" << std::endl;
        exit(1);
    }

    // Start client commands executor daemon
    try
    {
        size_t timeout = boost::lexical_cast<size_t>(argv[2]);

        // create Server
        server_ptr = std::make_shared<Server>(port, server_thread_pool_size, config_file_name, timeout);

        // Signal handler initializing for change signal actions
        struct sigaction sa;
        sa.sa_sigaction = &child_exit_handler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        sigaction(SIGCHLD, &sa, nullptr);   // when SIGCHLD signal become, execute sa_sigaction function

        server_ptr->run();  // run daemon server
        exit(0);
    }
    catch (const boost::bad_lexical_cast& e)
    {
        std::cerr << "Bad timeout value" << e.what() << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Something bad happened" << e.what() << std::endl;
    }

    return 1;
}
