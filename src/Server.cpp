#include <stdexcept>

#include "Server.h"

using boost::asio::ip::tcp;
using boost::asio::local::stream_protocol;

// ctor
Server::Server(short port, size_t thread_pool_size, const char* cmd_config_file, size_t timeout)
    : m_thread_pool_size(thread_pool_size)
    , m_timeout(timeout)
    , m_quit_signals(m_io_service)
    , m_update_config_signal(m_io_service)
    , m_tcp_acceptor(m_io_service)
    , m_tcp_endpoint(tcp::endpoint(tcp::v4(), port))
    , m_config_parser(cmd_config_file)
    , m_config(m_config_parser.parse_config())
{
    if (m_config.empty()) { throw logic_error("Config is empty! There is no commands to launch!"); }

    // Setting quit signals
    m_quit_signals.add(SIGINT);
    m_quit_signals.add(SIGTERM);

    #ifdef BOOST_SIGQUIT
    m_quit_signals.add(SIGQUIT);
    #endif

    // Signal for config updating
    m_update_config_signal.add(SIGHUP);

    // Setting handlers for signals
    m_quit_signals.async_wait(boost::bind(&Server::handle_stop, this));
    m_update_config_signal.async_wait(boost::bind(&Server::handle_update_config, this));

    // Configure endpoints and start listening for clients connections
    configure_tcp_endpoint();
}
// config sockaddr
void Server::configure_tcp_endpoint()
{
    // trying to bind tcp endpoint
    boost::system::error_code ec;
    m_tcp_acceptor.open(m_tcp_endpoint.protocol());
    m_tcp_acceptor.set_option(tcp::acceptor::reuse_address(true));
    m_tcp_acceptor.bind(m_tcp_endpoint, ec);

    if (ec) { throw logic_error("Port is already used. "); }

    m_tcp_acceptor.listen();    // listen for clients
    tcp_accept();
}

// run Server, starts clients threads with io_service::run()
void Server::run()
{
    // Initialize workers threads pool
    vector<shared_ptr<boost::thread>> threads;

    for (size_t i = 0; i < m_thread_pool_size; ++i)
    {
        shared_ptr<boost::thread> thread_ptr(
            new boost::thread(boost::bind(&boost::asio::io_service::run, &m_io_service)));
        threads.push_back(thread_ptr);
    }

    // wait for all threads execution has completed
    for (auto& thread : threads)
        thread->join();
}

void Server::process_child_exit(pid_t pid)
{
    boost::unique_lock<boost::mutex> lock(m_signal_mutex);

    auto it = m_pid_to_session_map.find(pid);   // get session by pid
    if (it != m_pid_to_session_map.end())
    {
        auto session = m_pid_to_session_map.at(pid);    // pid session

        // Need to erase element from map before handling child exit, because someone can obtain same pid
        lock.unlock();

        m_pid_to_session_map.erase(it->first);
        session->handle_child_exit();               // try to launch cmd process from cmd_queue
    }
}

void Server::tcp_accept()
{
    SyncData sync_data(m_config, m_config_mutex, m_pid_to_session_map, m_signal_mutex);
    auto session = make_shared<Session<tcp::socket>>(m_io_service, m_timeout, sync_data);  // create new session to accept

    // wait for clients connect to server
    m_tcp_acceptor.async_accept(session->socket(),
        [this, session](boost::system::error_code ec)
        {
            if (!ec) session->start();
            tcp_accept();                   // go on accepting new sessions
        });
}

void Server::handle_update_config()
{
    m_update_config_signal.async_wait(boost::bind(&Server::handle_update_config, this));
    boost::unique_lock<boost::shared_mutex> lock(m_config_mutex);                           // lock config writer
    m_config = m_config_parser.parse_config();
}

void Server::handle_stop()
{
    m_io_service.stop();
}
