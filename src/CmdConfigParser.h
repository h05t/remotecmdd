#ifndef CONFIG_PARSER_H
#define CONFIG_PARSER_H

#include <vector>
#include <string>

#include "SyncData.h"

using namespace std;

class CmdConfigParser
{
public:
    CmdConfigParser(const string& config_name);

    // Парсинг конфиг. файла команд пользователя
    // возврашает пустой конфиг, если формат файл неверный
    // формат файла: <cmd> <program>
    map<string, string> parse_config() const;

private:
    string m_config_name;
};

#endif // CONFIG_PARSER_H
