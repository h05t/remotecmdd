#include <fstream>
#include <sstream>

#include "CmdConfigParser.h"


CmdConfigParser::CmdConfigParser(const string& config_name)
    : m_config_name(config_name)
{
}

// TODO: exeptions handling
// parse cmd config
map<string, string> CmdConfigParser::parse_config() const
{
    ifstream in(m_config_name);

    map<string, string> config_data;
    if (!in) return config_data; // file does not exist

    string line;
    while (getline(in, line))
    {
        stringstream stream(line);
        string cmd;
        string program;
        stream >> cmd;
        stream >> program;
        if (!cmd.empty() && !program.empty()) {
            config_data[cmd] = program;
        }
    }

    return config_data;
}

