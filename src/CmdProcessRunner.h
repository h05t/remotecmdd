#ifndef PROCESS_RUNNER_H
#define PROCESS_RUNNER_H

#include <memory>
#include <vector>
#include <queue>
#include <string>
#include <utility>

#include <boost/thread.hpp>
#include <boost/atomic.hpp>

#include "SyncData.h"


using namespace std;

//
// Class that run client process with command
//
class CmdProcessRunner
{

public:
    CmdProcessRunner(const SyncData& sync_data);

    // Needed for wrapping attempt_launch method return value
    struct CmdStatus
    {
        bool attempted;
        bool launched;
        size_t task_id;

        CmdStatus(bool attempted, bool launched, size_t task_id)
            : attempted(attempted), launched(launched), task_id(task_id)
        { }
    };

    // Appends data to data buffer
    // After that performs parsing and enqueues command to command queue (if command was parsed)
    void commit_data(const string& data);

    // returns AttemptResult struct in which:
    //    'attempted' is true if child launch attempted,
    //    'launched' is true if child launched successfully
    //    'task_id' - launched task id
    CmdStatus cmd_process_launch();

    // writes stdout and stderr to corresponding arguments. Returns child exit code
    int write_execution_result(vector<char>& stdout_buf, vector<char>& stderr_buf);

    // kills child task if 'id' equals to current task id
    void kill_task(size_t id);

    // need this method because of 'chicken & egg' problem
    void initialize_with_session(const shared_ptr<BaseSession>& session);

private:
    string m_data;                     // Data buffer
    queue<string> m_cmd_queue;    // Commands buffer
    boost::mutex m_queue_mutex;             // Command queue sync stuff
    const map<string, string>& m_config; // Reference to config data
    boost::shared_mutex& m_config_mutex;    // Mutex for reader lock

    map<pid_t, shared_ptr<BaseSession>>& m_pid_to_session_map;    // SIGCHLD Dispatching stuff
    boost::mutex& m_signal_mutex;

    shared_ptr<BaseSession> m_session; // Current session
    boost::atomic<bool> m_is_running;       // Child sync stuff
    boost::atomic<pid_t> m_pid;             // Child process identifier
    boost::atomic<size_t> m_task_id;        // Current child task id
    boost::mutex m_child_mutex;             // Mutex for child shared data
    boost::atomic<FILE*> m_atomic_stdout;   // Standard output file struct pointer wrapper
    boost::atomic<FILE*> m_atomic_stderr;   // Standard error file struct pointer wrapper

    enum { buffer_length = 1024 };
    char buf_[buffer_length];


    // commands parsing
    vector<string> tokenize_cmd(const string& cmd) const;
    pair<bool, string> search_cmd(const string& cmd);
    // cmd execution utils
    void set_parent_descriptors(int pipe_stdout[2], int pipe_stderr[2]);
    void set_child_descriptors(int pipe_stdout[2], int pipe_stderr[2]);
    pid_t exec_and_bind_streams(const vector<string>& args);
    char** create_argv(const vector<string>& args) const;
    //
    void clear_context();
    void read_file_to_buf(vector<char>& result, FILE* file);
};

#endif // PROCESS_RUNNER_H
