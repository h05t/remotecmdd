#ifndef TYPES_H
#define TYPES_H

#include <vector>
#include <map>
#include <memory>
#include <string>

#include <boost/thread/mutex.hpp>
#include <boost/thread/shared_mutex.hpp>

#include "BaseSession.h"


// a wrapper on synchronization stuff & shared data
struct SyncData
{
    const std::map<std::string, std::string>& config;                   // commands config
    boost::shared_mutex& config_mutex;                                  // cmd config locker
    std::map<pid_t, std::shared_ptr<BaseSession>>& pid_to_session_map;  // clients Sessions by process pid
    boost::mutex& signal_mutex;                                         //

    SyncData(const std::map<std::string, std::string>& config,
             boost::shared_mutex& config_mutex,
             std::map<pid_t, std::shared_ptr<BaseSession>>& pid_to_session_map,
             boost::mutex& signal_mutex)
        : config(config)
        , config_mutex(config_mutex)
        , pid_to_session_map(pid_to_session_map)
        , signal_mutex(signal_mutex)
    {
    }
};

#endif
