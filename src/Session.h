#ifndef SESSION_H
#define SESSION_H

#include <memory>

#include <boost/asio.hpp>

#include "SyncData.h"
#include "BaseSession.h"
#include "CmdProcessRunner.h"


using namespace std;

//
// Server clients Session class, that may be shared_ptr or weak_ptr (using enable_shared_from_this)
//      - Socket is a template typename for other socket classes
//
template <typename Socket>
class Session : public enable_shared_from_this<Session<Socket>>, public BaseSession
{

public:
    Session(boost::asio::io_service& io_service, size_t m_timeout, const SyncData& sync_data);
    virtual ~Session() = default;

    // noncopyable
    Session(const Session&) = delete;
    Session& operator = (const Session&) = delete;

    // starts async read operation
    void start();
    // session socket reference, needed for accepting session
    Socket& socket();

private:
    boost::asio::io_service::strand m_strand;   // boost::strand needs for synchronization callback handlers in multithreaded program
    Socket m_socket;                            // session type
    CmdProcessRunner m_process_runner;          // Child process runner
    boost::asio::deadline_timer m_timer;        // Timer for killing child
    boost::posix_time::seconds m_timeout;       // Child timeout

    enum { buffer_length = 1024 };
    char data_[buffer_length];


    void do_read();
    void do_write(const string& data);             // write data to stream
    void do_write(const vector<char>& buffer);     //
    void try_launch_process();

    virtual void handle_child_exit();
};

template<class T>
Session<T>::Session(boost::asio::io_service& io_service, size_t m_timeout, const SyncData& sync_data)
    : m_strand(io_service)
    , m_socket(io_service)
    , m_process_runner(sync_data)
    , m_timer(io_service)
    , m_timeout(m_timeout)
{
}

template<class T>
void Session<T>::start()
{
    m_process_runner.initialize_with_session(this->shared_from_this());
    do_read();  // start read data asynchronously and try to launch commands when read data from socket
}

template<class Socket>
Socket& Session<Socket>::socket()
{
    return m_socket;
}
//
// 1. When session start, we start async read from socket
// 2. When push data to cmd buffer, try to launch cmd process and same as ...
//
template<class T>
void Session<T>::do_read()
{
    auto self(this->shared_from_this());    // get session shared pointer

    // async read read data from the stream socket
    m_socket.async_read_some(boost::asio::buffer(data_, buffer_length),
        m_strand.wrap(  // new handler that automatically dispatches the wrapped handler on the strand
        [this, self](boost::system::error_code ec, size_t length) // lymbda handler for async read
        {
            if (!ec) {                                                  // if no errors
                m_process_runner.commit_data(string(data_, length));    // push data to buffer
                try_launch_process();                                   // launch cmd
                do_read();                                              // when same as recursively
            }
        }));
}

// try to launch cmd
template<class T>
void Session<T>::try_launch_process()
{
    auto result = m_process_runner.cmd_process_launch();    // get cmd, check it, create process, exec it and return CmdStatus result
    auto task_id = result.task_id;

    if (result.launched)
    {
        //string msg = "Command launching...\n"; do_write(msg);
        //
        auto self(this->shared_from_this());    // get pointer
        m_timer.expires_from_now(m_timeout);
        //
        m_timer.async_wait( // async wait on timer
            m_strand.wrap([this, self, task_id](boost::system::error_code ec) { // new handler for strand
            // Check if timer was not cancelled
            if (ec != boost::asio::error::operation_aborted) {
                m_process_runner.kill_task(task_id);
            }
        }));
    }
    else if (result.attempted)
    {
        // Attempt to launch process failed
        string error_msg = "Unavailable command! Try again!\n";
        do_write(error_msg);
    }
}

template<class T>
void Session<T>::do_write(const string& data)
{
    do_write(vector<char>(data.c_str(), data.c_str() + data.length() + 1));
}

// async write buffer to socket
template<class T>
void Session<T>::do_write(const vector<char>& buffer)
{
    if (buffer.empty())
        return;

    auto self(this->shared_from_this());
    auto data_ptr = make_shared<vector<char>>(buffer);
    boost::asio::async_write(m_socket, boost::asio::buffer(&(*data_ptr)[0], data_ptr->size()),
        m_strand.wrap([this, self, data_ptr](boost::system::error_code ec, size_t) {}));
}

template<class T>
void Session<T>::handle_child_exit()
{
    // SIGCHLD received
    m_timer.cancel();

    vector<char> stdout, stderr;
    auto status = m_process_runner.write_execution_result(stdout, stderr);
    if (!status)
    {
        // All is OK, writing stdout to client
        do_write("Command successfully executed!\n");
    }
    else
    {
        string error_msg = "Command execution error. Exit code: ";
        error_msg += to_string(status);
        error_msg += "\n";
        do_write(error_msg);
    }
    do_write("Command output:\n");          do_write(stdout);
    do_write("Command error output:\n");    do_write(stderr);

    // launch commands from cmd_queue
    try_launch_process();
}

#endif // SESSION_H
