#include <sys/wait.h>
#include <csignal>
#include <cstdio>
#include <algorithm>

#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

#include "CmdProcessRunner.h"

CmdProcessRunner::CmdProcessRunner(const SyncData& sync_data)
    : m_config(sync_data.config)
    , m_config_mutex(sync_data.config_mutex)
    , m_pid_to_session_map(sync_data.pid_to_session_map)
    , m_signal_mutex(sync_data.signal_mutex)
    , m_is_running(false)
    , m_pid(-1)
    , m_task_id(0)
    , m_atomic_stdout(nullptr)
    , m_atomic_stderr(nullptr)
{
}

// get commands from data and push to cmd_queue
void CmdProcessRunner::commit_data(const string& data)
{
    m_data += data;
    size_t pos = m_data.find_first_of('\n');
    if (pos != string::npos)
    {
        auto cmd = m_data.substr(0, pos);   // extract cmd from buffer
        boost::algorithm::trim(cmd);        // remove whitespace symbols from cmd
        m_data.erase(0, pos + 1);           // erase current cmd from buffer
        if (cmd.empty()) return;            // ignore empty commands

        boost::unique_lock<boost::mutex> lock(m_queue_mutex);    // lock cmd queue
        m_cmd_queue.push(cmd);              // push cmd to cmd_queue
    }
}

// checking command
vector<string> CmdProcessRunner::tokenize_cmd(const string& cmd) const
{
    boost::char_separator<char> sep(" : ");
    boost::tokenizer<boost::char_separator<char>> tokenizer(cmd, sep);

    vector<string> args;
    auto end = tokenizer.end();

    //
    if (tokenizer.begin() == end)
        args.push_back("");
    else
        for (auto it = tokenizer.begin(); it != end; ++it)
            args.push_back(*it);

    return args;
}

// pair.first = true if command was found
pair<bool, string> CmdProcessRunner::search_cmd(const string& cmd)
{
    // config reader lock
    boost::shared_lock<boost::shared_mutex> lock(m_config_mutex);
    // Search for match
    auto found = m_config.find(cmd) != m_config.end();
    return found ? make_pair(true, m_config.at(cmd)) : make_pair(false, "");
}

// try to launch command
CmdProcessRunner::CmdStatus CmdProcessRunner::cmd_process_launch()
{
    boost::unique_lock<boost::mutex> queue_lock(m_queue_mutex);
    boost::unique_lock<boost::mutex> child_lock(m_child_mutex);

    if (m_is_running || m_cmd_queue.empty())  // Child is already running or nothing to execute
        return CmdStatus(false, false, m_task_id);

    auto cmd = m_cmd_queue.front(); // get cmd
    m_cmd_queue.pop();
    queue_lock.unlock();

    // Checking command
    auto args = tokenize_cmd(cmd);
    if (args.empty()) return CmdStatus(true, false, m_task_id); // Command is invalid

    auto search_result = search_cmd(args[0]);
    if (!search_result.first) return CmdStatus(true, false, m_task_id);

    auto program = search_result.second;
    args[0] = program;

    // Need to lock because of possible race conditions with SIGCHLD receiving
    boost::unique_lock<boost::mutex> signal_lock(m_signal_mutex);

    // CMD PROCESS LAUNCH
    // Create pipe, fork, exec and acquire child's stdout file struct pointer
    auto pid = exec_and_bind_streams(args);
    if (pid == -1) return CmdStatus(true, false, m_task_id); // Launch failed

    // Launch successful
    m_is_running = true;
    m_pid = pid;
    // Register pid for future SIGCHLD dispatching
    m_pid_to_session_map[pid] = m_session;
    return CmdStatus(true, true, m_task_id);
}

// WHAT ?
void CmdProcessRunner::set_parent_descriptors(int pipe_stdout[2], int pipe_stderr[2])
{
    close(pipe_stdout[1]);
    close(pipe_stderr[1]);
    m_atomic_stdout = fdopen(pipe_stdout[0], "r");  // open out stream for read
    m_atomic_stderr = fdopen(pipe_stderr[0], "r");  // open err stream for read
}

// WHAT ?
void CmdProcessRunner::set_child_descriptors(int pipe_stdout[2], int pipe_stderr[2])
{
    close(pipe_stdout[0]);
    close(pipe_stderr[0]);
    dup2(pipe_stdout[1], STDOUT_FILENO);
    dup2(pipe_stderr[1], STDERR_FILENO);
    close(pipe_stdout[1]);
    close(pipe_stderr[1]);
}

// Must be synchronized
pid_t CmdProcessRunner::exec_and_bind_streams(const vector<string>& args)
{
    // Creating pipe
    const char* program = args[0].c_str();
    int pipe_stdout[2];
    int pipe_stderr[2];

    if (pipe(pipe_stdout) || pipe(pipe_stderr)) return -1;  // Pipe error occured

    auto pid = fork();          // create cmd process
    if (pid < 0) return -1;

    if (pid != 0)
    {
        set_parent_descriptors(pipe_stdout, pipe_stderr);
        return pid;
    }
    else
    {
        // Need to unlock captured mutexes
        m_child_mutex.unlock();
        m_signal_mutex.unlock();

        // We are in the child
        set_child_descriptors(pipe_stdout, pipe_stderr);

        // Copy argv for new process executing
        char** argv = create_argv(args);
        execv(program, argv);
        perror("child");
        exit(1);
    }
}

// cmd process args
char** CmdProcessRunner::create_argv(const vector<string>& args) const
{
    char** argv = new char*[args.size() + 1];
    for (size_t i = 0; i < args.size(); ++i)
    {
        argv[i] = new char[args[i].length() + 1];
        strcpy(argv[i], args[i].c_str());
    }
    // Arguments must be guarded by NULL
    argv[args.size()] = nullptr;
    return argv;
}

//
// write command execution result to stdout and stderr streams
//
int CmdProcessRunner::write_execution_result(vector<char>& stdout_buf, vector<char>& stderr_buf)
{
    boost::unique_lock<boost::mutex> lock(m_child_mutex);
    if (m_pid == -1) return 1;

    int status;
    // Obtain child exit code
    waitpid(m_pid, &status, 0);

    // Copy file struct pointers
    FILE* stdout = m_atomic_stdout;
    FILE* stderr = m_atomic_stderr;
    // Clear context for the next launch
    clear_context();
    // Ready for new task!
    ++m_task_id;
    lock.unlock();

    if (stdout && stderr)
    {
        // Now we can read child's stdout and stderr
        read_file_to_buf(stdout_buf, stdout);
        read_file_to_buf(stderr_buf, stderr);

        fclose(stdout);
        fclose(stderr);
    }
    return status;
}

// Must be synchronized
void CmdProcessRunner::clear_context()
{
    m_is_running = false;
    m_atomic_stdout = 0;
    m_atomic_stderr = 0;
    m_pid = -1;
}

void CmdProcessRunner::read_file_to_buf(vector<char>& result, FILE* file)
{
    while (!feof(file))
    {
        size_t bytes_read = 0;
        if ((bytes_read = fread(buf_, sizeof(char), buffer_length, file)) != 0)
            for (size_t i = 0; i < bytes_read; ++i)
                result.push_back(buf_[i]);
    }
}

void CmdProcessRunner::kill_task(size_t id)
{
    boost::unique_lock<boost::mutex> lock(m_child_mutex);
    if (id == m_task_id && m_pid != -1)
        kill(m_pid, SIGKILL);
}

void CmdProcessRunner::initialize_with_session(const shared_ptr<BaseSession>& session)
{
    m_session = session;
}
