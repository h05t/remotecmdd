# RCMD - my first boost::asio project

Remote commands executor daemon, that store clients accepted commands list and launch it.

## Projects status - not fully tested!

## Requirements

* Boost 1.6.0
* Codeblocks IDE

## Systems

* all POSIX-based systems

## Commands file format:

<pre>
  ls : /bin/ls
  cat : /bin/cat
  pwd : /bin/pwd
  ...
</pre>

## How to launch daemon:

./RCMD <cmd_config_list> <cmd_timeout>

## Connect and launch: 

<pre>
  nc localhost 12345
  ls ~
</pre>

or

<pre>
  telnet localhost 12345
  cat /etc/environment
</pre>

